#include <iostream>
#include <fstream>
#include <stdio.h>
#include <process.h>
#include <string>

/*
    stockManagementSystem.cpp
    author: M00605010
    created: 13/01/2020
    updated: 13/01/2020
*/
class item // item class
{
    int item_ID;
    std::string item_name;
    double price;
    int quantity;
public:
    void itemData() // Addition of items
    {
        system("cls");
        std::cout << "\nEnter Item ID: \t";
        std::cin >> item_ID;
        std::cout << "\nEnter Item Name: \t";
        std::cin >> item_name;
        std::cout << "\n Enter Item Price (GBP): \t";
        std::cin >> price;
        std::cout << "Enter amount of items: ";
        std::cin >> quantity;

    } 

    void showItemData() // show items
    {
        system("cls");
        std::cout << "'\nThe Item ID: " << item_ID;
        std::cout << "'\nThe Item Name: " << item_name;
        std::cout << "'\nThe Item Price: " << price;
        std::cout << "'\nThe Item Quantity: " << quantity;
    }

    // return value for item data
    int returnItemID() 
    {
        return item_ID;
    }    
    std::string returnItemName()
    {
        return item_name;
    }       
    double returnItemPrice()
    {
        return price;
    }    
    int returnItemQuantity()
    {
        return quantity;
    }
}; // end of item class

std::fstream file; // global declaration for stream and object
item file_item;

void administrators_menu() // admin Menu
{
    system("cls");
    int adminSelection;
    do { 
    std::cout << "\n\t Administrator's Menu";
    std::cout << "\n\t 1: View report of sales";
    std::cout << "\n\t 2: Add Items";
    std::cout << "\n\t 3: Restock Items";
    std::cout << "\n\t 4: Update stock quantity";
    std::cout << "\n\t 5: Load/Save Stock Data";
    std::cout << "\n\t 6: Exit";
    std::cout << "\n\n\t Please choose your option ( 1 - 5 ): ";
    std::cin >> adminSelection;

    switch(adminSelection){
        case 6: exit(0);
            break; 
        default: std::cout << "\a";
    }
 } while (adminSelection != 5);

}

int main() // main function of program
{ 
    int userSelection;
    do {
        system("cls"); // clear screen when looped
        std::cout << "\n\n\n\t Main menu";
        std::cout << "\n\t 1: Administrator";
        std::cout << "\n\t 2: Customer";
        std::cout << "\n\t 3: Exit";
        std::cout << "\n\n\t Please choose your option ( 1 - 3 ): ";
        std::cin >> userSelection;
        switch(userSelection){
            case 1: administrators_menu();
                break;
            case 3: exit(0); // exits program || from stdio
            default: std::cout << "\a";
        }
    } while (userSelection != 3); 
    return 0;
}